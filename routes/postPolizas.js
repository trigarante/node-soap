var express = require('express');
var router = express.Router();
var soap = require('soap');

/* GET users listing. */
router.get('/', function(req, res) {
  var barcode = req.query.barcode || req.body.barcode;
  var wsdlUrl = 'http://www.searchupc.com/service/UPCSearch.asmx?wsdl';
  soap.createClient(wsdlUrl, function(err, soapClient){
    // we now have a soapClient - we also need to make sure there's no `err` here.
    if (err){
      return res.status(500).json(err);
    }
    soapClient.GetProduct({
      upc : barcode,
      accesstoken : '924646BB-A268-4007-9D87-2CE3084B47BC'
    }, function(err, result){
      if (err){
        return res.status(500).json(err);
      }
      return res.json(result);
    });
  });
});

router.get('/test', function (req, res) {
  var wsdlUrl = 'http://pagosqa.qualitas.com.mx/ws/wsCollection.php?WSDL';
  var wsdlOptions = {
    valueKey: '$value',
    xmlKey: '$xml'
  };
  soap.createClient(wsdlUrl, wsdlOptions, function (err, soapClient,r) {
    // we now have a soapClient - we also need to make sure there's no `err` here.
    if (err) {
      return res.status(500).json(err);
    }
    const xmlArgs = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><oplCollection><Collection NoPoliza=\"7160154210\" NoNegocio=\"02886\" wpuid=\"bb832dad623e9cf6a4172d746f8f2af3\" wptoken=\"5bfb9c819296b6bb8da6410696e4fffd\"><collectionData><type>C</type><userKey>PISM950928</userKey><name>Miguel Pineda</name><crypto>0</crypto><number>5477123412341234</number><bankcode>12</bankcode><expmonth>11</expmonth><expyear>2021</expyear></collectionData><insuranceData><akey>35863</akey><email>ihernandez@trigarante.com</email><PlazoPago>C</PlazoPago><currency>mxn</currency></insuranceData></Collection></oplCollection>"

    soapClient.oplCollection({
      "xml_collection": {
        $value: xmlArgs
      }
    },function(err, result, soapHeader){
      console.log('last request: ', soapClient.lastRequest)
      if (err){
        console.log(soapHeader);
        return res.status(500).json(err);
      }
      return result;
    })
  });
});
module.exports = router;
